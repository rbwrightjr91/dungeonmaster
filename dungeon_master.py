import datetime
import logging
import os

import asyncpg
from discord.ext import commands

dm_events = (
    'events.on_ready',
    'events.close',
    'events.on_guild_join',
    'events.on_command_error',
)

dm_commands = (
    'commands.stats',
    'commands.config',
    'commands.spells'
)


class DungeonMaster(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.uptime = datetime.datetime.utcnow()

        logging.basicConfig(
            filename='dungeon_master.log',
            level=logging.DEBUG,
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
        )

        self.logger = logging.getLogger(__name__)
        stdout = logging.StreamHandler()
        stdout.setFormatter(
            logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        )
        stdout.setLevel(logging.DEBUG)
        self.logger.addHandler(stdout)
        self.logger.info('Starting up...')

        self.db = kwargs.pop('db')
        self.srd_api_url = 'https://www.dnd5eapi.co/api'

        self.load_events()
        self.load_commands()

    def load_events(self):
        self.logger.info('Loading events...')
        for event in dm_events:
            try:
                self.load_extension(event)
            except Exception as e:
                self.logger.error(f'Failed to load event {event}.', exc_info=True)
        self.logger.info('Events loaded.')

    def load_commands(self):
        self.logger.info('Loading commands...')
        for command in dm_commands:
            try:
                self.load_extension(command)
            except Exception as e:
                self.logger.error(f'Failed to load command {command}.', exc_info=True)
        self.logger.info('Commands loaded.')
