import json

import asyncpg
from discord.ext import commands


class OnGuildJoin(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        gid = guild.id
        d = await self.bot.db.fetchrow(f"SELECT gid FROM dungeon_master.guild WHERE gid='{gid}'");
        if d:
            self.bot.logger.info(f'Guild {gid} exists.')
            return
        self.bot.logger.info(f'Inserting {gid} into db...')
        try:
            # with open('configs/base-config.json') as base, open(f'configs/config-{gid}.json', 'w') as f:
            #     json.dump(json.load(base), f)

            await self.bot.db.execute(f"INSERT INTO dungeon_master.guild(gid, prefix, dm_emoji) VALUES ({gid}, '&', ':man_mage:');")

            self.bot.logger.info(f'{gid} inserted.')
        except Exception as e:
            self.bot.logger.error(f'Couldn\'t insert server {gid}', exc_info=True)


def setup(bot):
    try:
        bot.add_cog(OnGuildJoin(bot))
        bot.logger.info(f'{__name__} event cog loaded.')
    except Exception as e:
        bot.logger.error(f'Couldn\'t load event cog {__name__}', exc_info=True)