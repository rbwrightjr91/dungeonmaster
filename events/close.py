from discord.ext import commands


class Close(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def close(self):
        self.bot.logger.info(f'{self.user.name} shutting down...')
        self.bot.db.close()
        await self.bot.close()


def setup(bot):
    try:
        bot.add_cog(Close(bot))
        bot.logger.info(f'{__name__} event cog loaded.');
    except Exception as e:
        bot.logger.error(f'Couldn\'t load event cog {__name__}.', exc_info=True)