import discord
from discord.ext import commands


class OnReady(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        await self.bot.change_presence(activity=discord.Game(name='Plotting a TPK'))
        self.bot.logger.info(f'{self.bot.user.name} (ID: {self.bot.user.id}) ready!')


def setup(bot):
    try:
        bot.add_cog(OnReady(bot))
        bot.logger.info(f'{__name__} event cog loaded.');
    except Exception as e:
        bot.logger.error(f'Couldn\'t load event cog {__name__}', exc_info=True)