import discord
from discord.ext import commands
from utils import get_server_emoji

class OnCommandError(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if hasattr(ctx.cog, f'_{ctx.cog.__class__.__name__}_error'.lower()):
            return

        emoji = await get_server_emoji(db=self.bot.db, guild=ctx.guild)

        if isinstance(error, commands.NoPrivateMessage):
            await ctx.author.send(f'{emoji} This command cannot be used in private messages.')
            return

        self.bot.logger.debug(f'({ctx.guild.id}) {type(error).__name__}: {error}', exc_info=True)
        if isinstance(error, commands.DisabledCommand):
            await ctx.author.send(f'{emoji} Sorry. This command is disabled and cannot be used.')
        elif isinstance(error, commands.CommandInvokeError):
            original = error.original
            if not isinstance(original, discord.HTTPException):
                self.bot.logger.error(f'In {ctx.command.qualified_name}:', exc_info=True)
                self.bot.logger.error(f'{original.__class__.__name__}: {original}')
        else:
            await ctx.send(f'{emoji} {error}')

def setup(bot):
    try:
        bot.add_cog(OnCommandError(bot))
        bot.logger.info(f'{__name__} event cog loaded.');
    except Exception as e:
        bot.logger.error(f'Couldn\'t load event cog {__name__}', exc_info=True)
