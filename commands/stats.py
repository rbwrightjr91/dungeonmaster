import datetime

from discord.ext import commands


def days_hours_minutes(timedelta : datetime.timedelta):
    return timedelta.days, timedelta.seconds // 3600, (timedelta.seconds // 60) % 60, timedelta.seconds % 60


class Stats(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="stats", description="Bot statistics")
    async def stats(self, ctx):
        now = datetime.datetime.utcnow()
        now = now.replace(microsecond=0)

        uptime = self.bot.uptime
        uptime = uptime.replace(microsecond=0)

        days, hours, minutes, seconds = days_hours_minutes(now - uptime);
        await ctx.send(f'Uptime: {days} days, {hours} hours, {minutes} minutes, {seconds} seconds')


def setup(bot):
    try:
        bot.add_cog(Stats(bot))
        bot.logger.info(f'{__name__} command cog loaded.');
    except Exception as e:
        bot.logger.error(f'Couldn\'t load command cog {__name__}', exc_info=True)