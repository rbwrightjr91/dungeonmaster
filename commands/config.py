import discord
from discord.ext import commands
from utils import get_server_emoji


# Cog to handle commands related to bot configuration
class Config(commands.Cog, command_attrs=dict(hidden=True)):

    def __init__(self, bot):
        self.bot = bot

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(administrator=True))
    async def config(self, ctx, key, value=None):
        gid = ctx.guild.id
        emoji = await get_server_emoji(db=self.bot.db, gid=gid)

        if not value:
            await ctx.send(f'{emoji} Please specify a value!')
            return

        await self.bot.db.execute(f"UPDATE dungeon_master.guild SET {key}='{value}' WHERE gid='{gid}'")

        emoji = emoji if not key == 'dm_emoji' else value
        await ctx.send(f'{emoji} My spellbook has been updated!')

    @config.command()
    @commands.guild_only()
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(administrator=True))
    async def show(self, ctx, *, key=None):
        gid = ctx.guild.id
        d = await self.bot.db.fetchrow(f"SELECT * FROM dungeon_master.guild WHERE gid='{gid}';")
        d = dict(d)

        if not key:

            embed = discord.Embed(
                title=f'{self.bot.user.name} Server Configuration'
            )

            for k, v in d.items():
                if k == 'id': continue
                embed.add_field(name=f'{k}', value=f'`{v}`')

            await ctx.send(embed=embed)
        elif key not in d:
            await ctx.send(f'{d["dm_emoji"]} My spellbook doesn\'t contain `{key}`!')
        else:
            await ctx.send(f'{d["dm_emoji"]} `{key}` is set to `{d[key]}` in my spellbook.')


def setup(bot):
    try:
        bot.add_cog(Config(bot))
        bot.logger.info(f'{__name__} command cog loaded.')
    except Exception as e:
        bot.logger.error(f'Couldn\'t load command cog {__name__}', exc_info=True)
