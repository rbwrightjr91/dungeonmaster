import aiohttp
import discord
from discord.ext import commands
from utils import get_server_emoji


def is_valid_level(level):
    return (level.lower() == 'cantrip' or level.lower() == 'cantrips') and (0 <= int(level) <= 20)


def level_format(level):
    level = str(level)
    if level == '0' or level.lower() == 'cantrip':
        return 'Cantrip'
    elif level[-1] == '1' and not level == '11':
        return f'{level}st Level Spell'
    elif level[-1] == '2' and not level == '12':
        return f'{level}nd Level Spell'
    elif level[-1] == '3' and not level == '13':
        return f'{level}rd Level Spell'
    else:
        return f'{level}th Level Spell'


# Cog to handle commands related to spells
class Spells(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self._original_help_command = bot.help_command

    @commands.group(invoke_without_command=True, description='All commands related to spells.')
    async def spells(self, ctx, *, spell_name: str):
        spell_raw = spell_name
        spell_name = str(spell_name).lower().replace(' ', '-')

        emoji = await get_server_emoji(guild=ctx.guild, db=self.bot.db)

        async with aiohttp.ClientSession() as session:
            async with session.get(url=f'{self.bot.srd_api_url}/spells/{spell_name}') as res:

                self.bot.logger.debug(f'{res.status} - {self.bot.srd_api_url}/spells/{spell_name}')

                if res.status > 399:
                    msg = f'{emoji} Ha! `{str(spell_raw).title()}` isn\'t a real spell. Do you take me for a fool!?'
                    await ctx.send(msg)
                    return

                spell = await res.json()

                spell_level = level_format(spell.get('level'))

                spell_desc = f'**{spell_level} - {spell.get("school").get("name")}**'

                if spell.get('ritual'):
                    spell_desc = f'{spell_desc}\n\n**Ritual**'

                if spell.get('concentration'):
                    spell_desc = f'{spell_desc}\n**Concentration**'

                for desc in spell.get('desc'):
                    spell_desc = f'{spell_desc}\n\n{desc}'

                if spell.get('higher_level', None):
                    spell_desc = f'{spell_desc}\n\n**Higher Levels:**'
                    for higher_level in spell.get('higher_level'):
                        spell_desc = f'{spell_desc} {higher_level}'

                spell_comp = ''
                for comp in spell.get('components'):
                    spell_comp += f'{comp} '

                embed = discord.Embed(
                    title=f'{spell.get("name")}',
                    description=f'{spell_desc}',
                )
                embed.add_field(name='Range', value=spell.get('range'))
                embed.add_field(name='Components', value=spell_comp)
                embed.add_field(name='Duration', value=spell.get('duration'))
                embed.add_field(name='Cast Time', value=spell.get('casting_time'))
                embed.add_field(name='Material', value=f'{spell.get("material", "No Material Component")}')
                embed.add_field(name='Classes', value='\n'.join([x['name'] for x in spell.get('classes')]))
                embed.set_footer(text=f'Source: {str(spell.get("page")).upper()}')
                await ctx.send(embed=embed)

    @spells.command()
    async def search(self, ctx, *, search_term=None):
        emoji = await get_server_emoji(guild=ctx.guild, db=self.bot.db)

        if not search_term:
            await ctx.send(f'{emoji} Please provide a search term.')
            return
        elif len(search_term) < 2:
            await ctx.send(f'{emoji} Search term must be at least 2 characters long.')
            return
        search_term = str(search_term)
        async with aiohttp.ClientSession() as session:
            async with session.get(url=f'{self.bot.srd_api_url}/spells') as res:

                self.bot.logger.debug(f'{res.status} - {self.bot.srd_api_url}/spells')

                if res.status > 399:
                    msg = f'{emoji} Hmm, I can\'t seem to find my spellbook at the moment...'
                    await ctx.send(msg)
                    return

                spell_res = await res.json()
                all_spells = [x['name'] for x in spell_res.get('results')]

        search_result = [s for s in all_spells if search_term.lower() in s.lower()]

        if len(search_result) == 0:
            msg = f'{emoji} Hmm, I can\'t seem to find anything related to `{search_term}` in my spellbook...'
            await ctx.send(msg)
            return

        str_result = '\n'.join(search_result)

        embed = discord.Embed(
            title=f'Spell Search',
            description=f'{len(search_result)} results for \'{search_term}\'\n\n{str_result}',
        )

        await ctx.send(embed=embed)

    @spells.command()
    async def level(self, ctx, *, level):
        emoji = await get_server_emoji(guild=ctx.guild, db=self.bot.db)

        if not is_valid_level(level):
            await ctx.send(f'{emoji} `level` needs to be between 0 and 20, or "cantrip"')
            return

        #TODO: Cache these if bot performance becomes an issue
        async with aiohttp.ClientSession() as session:
            async with session.get(url=f'{self.bot.srd_api_url}/classes/') as res:

                self.bot.logger.debug(f'{res.status} - {self.bot.srd_api_url}/classes')

                if res.status > 399:
                    msg = f'{emoji} Hmm, I can\'t seem to find my spellbook at the moment...'
                    await ctx.send(msg)
                    return

                classes = (await res.json()).get('results')

            level = '0' if level.lower() == 'cantrip' or level.lower() == 'cantrips' else level
            spells = []

            for c in classes:
                async with session.get(url=f'{self.bot.srd_api_url}/classes/{c.get("index")}/levels/{level}/spells/') as res:
                    if res.status > 399:
                        msg = f'{emoji} Hmm, I can\'t seem to find my spellbook at the moment...'
                        await ctx.send(msg)
                        return

                    spells += [s['name'] for s in (await res.json()).get('results')]

            spells = sorted(set(spells))


            str_result = '\n'.join(spells)

            embed = discord.Embed(
                title=f'{level_format(level)}s',
                description=f'{str_result}',
            )

            await ctx.send(embed=embed)

    @spells.error
    @search.error
    @level.error
    async def _spells_error(self, ctx, error):
        self.bot.logger.error(error, exc_info=True)
        emoji = await get_server_emoji(guild=ctx.guild, db=self.bot.db)
        await ctx.send(f'{emoji} {error}')

    @spells.command(name='help')
    async def _spell_help(self, ctx):
        cmds = ['spells help', 'spells', 'spells search', 'spells level']
        args = ['', '[spell_name]', '[search_term]', '[level]']
        desc = ['Prints this help dialog', 'Retrieve details of given spell', 'Search for spells containing search term',
                'List all spell names of the given level']

        help_msg = ''
        for i in range(0, len(cmds)):
            help_msg += f'\n\n`{cmds[i]} {args[i]}`: {desc[i]}'

        embed = discord.Embed(
            title='Spells Command',
            description=f'All Commands Related to Spells{help_msg}'
        )

        await ctx.send(embed=embed)

    def cog_unload(self):
        self.bot.help_command = self._original_help_command


def setup(bot):
    try:
        bot.add_cog(Spells(bot))
        bot.logger.info(f'{__name__} command cog loaded.')
    except Exception as e:
        bot.logger.error(f'Couldn\'t load command cog {__name__}', exc_info=True)
