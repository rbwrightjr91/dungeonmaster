async def get_server_emoji(**kwargs):
    guild = kwargs.pop('guild')
    if not guild:
        return ':man_mage:'

    db = kwargs.pop('db')

    return (await db.fetchrow(f"SELECT dm_emoji FROM dungeon_master.guild WHERE gid='{guild.id}'"))['dm_emoji']
