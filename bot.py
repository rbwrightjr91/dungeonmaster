import asyncio
import os

import asyncpg
from discord.ext import commands

from dungeon_master import DungeonMaster


async def get_prefix(bot, message):
    if not message.guild:
        return commands.when_mentioned_or('')(bot, message)

    gid = message.guild.id
    d = await bot.db.fetchrow(f"SELECT * FROM dungeon_master.guild WHERE gid='{gid}';")
    return commands.when_mentioned_or(d['prefix'])(bot, message)


async def run():
    db = await asyncpg.create_pool(dsn=os.environ['DATABASE_URL'])
    bot = DungeonMaster(command_prefix=get_prefix, db=db)

    try:
        await bot.start(os.environ['DISCORD_BOT_TOKEN'])
    except KeyboardInterrupt:
        await bot.close()


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(run())
